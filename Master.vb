﻿Public Class Master
    Public Shared Property Clock As Clock

    Private Shared Function CheckClock() As Boolean
        Return Clock IsNot Nothing
    End Function

    Public Shared Sub SetCorner(corner As Clock.corner)
        If CheckClock() Then Clock.ScreenCorner = corner
    End Sub
End Class
