﻿Imports System.ComponentModel
Imports System.Windows.Controls.Primitives

Public Class ColorPicker : Implements INotifyPropertyChanged
#Region "WOW"
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub
#End Region

    Public Shared ReadOnly TlacitkoProperty As DependencyProperty = DependencyProperty.Register("Tlacitko", GetType(ToggleButton), GetType(ColorPicker), New FrameworkPropertyMetadata(Nothing))

    Public Property Tlacitko As ToggleButton
        Get
            Return DirectCast(GetValue(TlacitkoProperty), ToggleButton)
        End Get
        Set(value As ToggleButton)
            SetValue(TlacitkoProperty, value)

            NotifyPropertyChanged()
        End Set
    End Property
End Class
