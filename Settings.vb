﻿Public Class Settings

    Public Class Names
        Public Shared ReadOnly Property Position As String
            Get
                Return "Position"
            End Get
        End Property

        Public Shared ReadOnly Property Transparency As String
            Get
                Return "Transparency"
            End Get
        End Property

        Public Shared ReadOnly Property TopMost As String
            Get
                Return "TopMost"
            End Get
        End Property

        Public Shared ReadOnly Property ClickThru As String
            Get
                Return "ClickThru"
            End Get
        End Property

        Public Shared ReadOnly Property ShowMiliseconds As String
            Get
                Return "ShowMiliseconds"
            End Get
        End Property
    End Class

    Private Shared Sub Log(txt As String)
        Trace.WriteLine(String.Format("[{0}] [Nastavení] {1}", Date.Now.ToString("HH:mm:ss:fff"), txt))
    End Sub

#Region "HELP"
    Public Shared Function BoolToString(bool As Boolean) As String
        Return If(bool, "True", "False")
    End Function

    Public Shared Function StringToBool(bool As String) As Boolean
        Return If(bool.ToLower.Trim = "true", True, False)
    End Function
#End Region

#Region "PROPS"
    Private Shared ReadOnly Property RegSavePath As String
        Get
            Return String.Format("Software\{0}\{1}", My.Application.Info.CompanyName, My.Application.Info.ProductName)
        End Get
    End Property
#End Region

#Region "VALS"
    Private Shared Settings As New Dictionary(Of String, String)
    Private Shared DefaultSettings As New Dictionary(Of String, String) From {{Names.Position, "RightBottom"}, {Names.TopMost, "True"}, {Names.Transparency, "0.5"}, {Names.ClickThru, "False"}, {Names.ShowMiliseconds, "False"}}
#End Region

#Region "DoStuff"
    Public Shared Sub ToSettings(name As String, value As String)
        If Settings.ContainsKey(name) Then
            'Není stejná
            If Not Settings(name) = value Then
                'Jen změnit honotu
                Settings(name) = value

                Log(String.Format("Změna hodnoty | {0} = {1}", name, value))
            Else
                Log(String.Format("Hodnota nezměněna | {0} = {1}", name, value))
            End If
        Else
            'Přímo přidat
            Settings.Add(name, value)

            Log(String.Format("Přidání hodnoty | {0} = {1}", name, value))
        End If
    End Sub

    Public Shared Function FromSettings(name As String) As String
        Dim value As String = Nothing

        If Settings.ContainsKey(name) Then
            value = Settings(name)

            Log(String.Format("Vrácení hodnoty | USER {0} = {1}", name, value))
            'Trace.WriteLine(String.Format("[Vrácení hodnoty] {0} = {1}", name, value))
        ElseIf DefaultSettings.ContainsKey(name) Then
            value = DefaultSettings(name)

            Log(String.Format("Vrácení hodnoty | DEFAULT {0} = {1}", name, value))
        Else
            Log(String.Format("Vrácení hodnoty | {0} neexistuje", name))
            'Trace.WriteLine(String.Format("[Vrácení hodnoty] {0} neexistuje", name))
        End If

        Return value
    End Function
#End Region

#Region "LOAD/SAVE"
    Public Shared Sub Load()
        'Otevřít registr
        Dim regRoot = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(RegSavePath, True)
        If regRoot Is Nothing Then
            Microsoft.Win32.Registry.CurrentUser.CreateSubKey(RegSavePath)
            regRoot = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(RegSavePath, True)
        End If

        Log("Načítání...")
        'Trace.WriteLine("[Nastavení] Načítání...")
        For Each setting In DefaultSettings
            'Podle klíče najít aktuální z registru
            Dim regVal = regRoot.GetValue(setting.Key)
            Dim valToSave = setting.Value 'Výchozí

            If regVal IsNot Nothing Then
                'TODO: Validovat
                If setting.Key = Names.Position Then
                    Try
                        [Enum].Parse(GetType(Clock.corner), regVal)
                        valToSave = regVal 'VALID
                    Catch ex As Exception
                        'No Report
                    End Try
                ElseIf setting.Key = Names.Transparency Then
                    Try
                        Dim convVal = Val(regVal)
                        If 0 <= convVal <= 1 Then
                            valToSave = regVal 'VALID
                        End If
                    Catch ex As Exception
                        'No Report
                    End Try
                ElseIf setting.Key = Names.ClickThru Or setting.Key = Names.TopMost Or setting.Key = Names.ShowMiliseconds Then
                    valToSave = BoolToString(StringToBool(regVal))
                End If
            Else
                regRoot.SetValue(setting.Key, setting.Value)
            End If

            ToSettings(setting.Key, valToSave)
        Next
        regRoot.Close()
        Log("Načteno!")
        'Trace.WriteLine("[Nastavení] Načteno!")
    End Sub

    

    Public Shared Sub Save()
        'Otevřít registr
        Dim regRoot = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(RegSavePath, True)
        If regRoot Is Nothing Then
            Microsoft.Win32.Registry.CurrentUser.CreateSubKey(RegSavePath)
            regRoot = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(RegSavePath, True)
        End If

        Log("Ukládání...")
        'Trace.WriteLine("[Nastavení] Ukládání...")
        For Each setting In Settings
            regRoot.SetValue(setting.Key, setting.Value)
        Next
        regRoot.Close()
        Log("Uloženo!")
        'Trace.WriteLine("[Nastavení] Uloženo!")
    End Sub
#End Region

End Class
