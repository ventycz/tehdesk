﻿Imports System.Windows.Threading
Imports Microsoft.Win32

Public Class Clock
#Region "Notify"
    Implements ComponentModel.INotifyPropertyChanged

    Public Event PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs) Implements ComponentModel.INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
    End Sub
#End Region

    Private ShowMS_DP As DependencyProperty = DependencyProperty.Register("ShowDP", GetType(Visibility), GetType(Clock), New PropertyMetadata(Visibility.Collapsed))

    Public Property ShowMS As Visibility
        Get
            Return GetValue(ShowMS_DP)
        End Get
        Set(value As Visibility)
            SetValue(ShowMS_DP, value)

            NotifyPropertyChanged()
        End Set
    End Property

    Public WriteOnly Property ClickThru As Boolean
        Set(value As Boolean)
            If value Then
                WindowsServices.ClickThru(New Interop.WindowInteropHelper(Me).Handle)
            Else
                WindowsServices.notclickThru(New Interop.WindowInteropHelper(Me).Handle)
            End If
        End Set
    End Property


    Public Property CanMove As Boolean = False

    Public Event ScreenChanged(e As ScreenChangeEventArgs)

    WithEvents timer As New DispatcherTimer With {.Interval = New TimeSpan(0, 0, 0, 0, 1)}

    Public Class ScreenChangeEventArgs
        Public SuccessFul As Boolean
        Public Index As Integer

        Sub New(Success As Boolean, Index As Integer)
            Me.SuccessFul = Success : Me.Index = Index
        End Sub
    End Class

    Public Property CanResize As Boolean
        Get
            Return If(Me.ResizeMode = Windows.ResizeMode.NoResize Or Me.ResizeMode = Windows.ResizeMode.CanMinimize, False, True)
        End Get
        Set(value As Boolean)
            Me.ResizeMode = If(value, ResizeMode.CanResize, ResizeMode.NoResize)
        End Set
    End Property

    Public Property Transparency As Double
        Get
            Return HolderBorder.Background.Opacity
        End Get
        Set(value As Double)
            HolderBorder.Background.Opacity = value
        End Set
    End Property

    Dim _ScreenCorner As corner = corner.RightBottom
    Property ScreenCorner As corner
        Get
            Return _ScreenCorner
        End Get
        Set(value As corner)
            _ScreenCorner = value

            Locate()
        End Set
    End Property


    Dim _ShowOnScreen As Integer
    Property ShowOnScreen As Integer
        Get
            Return _ShowOnScreen
        End Get
        Set(value As Integer)
            Dim oldIndex As Integer = _ShowOnScreen : Dim newIndex As Integer = value : Dim Success As Boolean = True

            'Je to index a není větší než možný
            If value < 0 Or value > Screens.Count - 1 Then
                newIndex = Array.FindIndex(Screens, Function(scr) scr.Primary = True) 'Finds Index Of Primary Monitor/Screen
                Success = False 'Yup, Failed
            End If

            _ShowOnScreen = newIndex 'Sets Get Value

            RaiseEvent ScreenChanged(New ScreenChangeEventArgs(Success, newIndex)) 'Shoots at Main Window

            Locate() 'Do WINDOW Stuff
        End Set
    End Property

    ReadOnly Property Screens As Windows.Forms.Screen()
        Get
            Return Windows.Forms.Screen.AllScreens
        End Get
    End Property

    Private Sub Clock_Activated(sender As Object, e As EventArgs) Handles Me.Activated

    End Sub

    Private Sub Clock_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Dim scr As Integer = If(Screens.Count > 1, 1, 0) 'Default
        ShowOnScreen = scr 'Set To Default
        AddHandler SystemEvents.DisplaySettingsChanged, AddressOf Locate 'Handle screen settings change
        timer.Start()
    End Sub

    Dim userLocChange As Boolean = False

    Dim lastLoc As Point = Nothing
    Private Sub Clock_LocationChanged(sender As Object, e As EventArgs) Handles Me.LocationChanged
        If Me.IsLoaded Then 'Už žádné 'InvalidOperationException'
            Dim parentRect = GetWindowScreen.WorkingArea 'WinForms
            Dim conv = New Rect(New Point(parentRect.Left, parentRect.Top), New Size(parentRect.Width, parentRect.Height)) 'WPF <3
            Dim newLoc As New Rect(Me.PointToScreen(New Point(0, 0)), New Size(Me.ActualWidth, Me.ActualHeight)) 'TOO FCKIN HARD

            Dim TR_good = conv.Contains(newLoc.TopRight), TL_good = conv.Contains(newLoc.TopLeft), BR_good = conv.Contains(newLoc.BottomRight), BL_good = conv.Contains(newLoc.BottomLeft)
            Dim Top_good = (TR_good Or TL_good), Bottom_good = (BR_good Or BL_good), Left_good = (TL_good Or BL_good), Right_good = (TR_good Or BR_good)
            Dim All_good = (TR_good And TL_good And BL_good And BR_good)

            'COLORZ
            Dim brush_good = Brushes.Green, brush_bad = Brushes.Red
            Rect_TL.Fill = If(TL_good, brush_good, brush_bad) : Rect_TR.Fill = If(TR_good, brush_good, brush_bad) : Rect_BL.Fill = If(BL_good, brush_good, brush_bad) : Rect_BR.Fill = If(BR_good, brush_good, brush_bad)
            'END COLORZ

            Dim fix_x = False, fix_y = False

            If All_good Then
                'WOW
                lastLoc = New Point(Me.Left, Me.Top)
            Else
                'Something failed
                fix_x = (Not Right_good Or Not Left_good)
                fix_y = (Not Top_good Or Not Bottom_good)

                If fix_x Then Me.Left = lastLoc.X
                If fix_y Then Me.Top = lastLoc.Y
            End If
        End If
    End Sub

    Private Sub Clock_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs) Handles Me.MouseLeftButtonDown
        If CanMove Then
            DragMove()
        End If
    End Sub

    Private Sub Clock_MouseMove(sender As Object, e As Input.MouseEventArgs) Handles Me.MouseMove
        If CanMove Then
            Me.Cursor = Cursors.SizeAll

            'If e.LeftButton = MouseButtonState.Pressed Then
            '    userLocChange = True
            '    DragMove()
            '    userLocChange = False
            'End If
        Else
            Me.Cursor = Cursors.Arrow
        End If
    End Sub

    Sub Locate()
        Dim TehPos As Point = GetCornerPoint(ScreenCorner, Screens(ShowOnScreen)) 'getRightPos(Screens(ShowOnScreen))

        Me.Left = TehPos.X
        Me.Top = TehPos.Y
    End Sub

    Function GetWindowScreen() As Windows.Forms.Screen
        Return Windows.Forms.Screen.FromHandle(WindowsServices.GetHandleFromWindow(Me))
    End Function

    Enum corner
        LeftTop
        LeftBottom
        RightTop
        RightBottom
        Manual
    End Enum
    Function GetCornerPoint(crn As corner, screen As Windows.Forms.Screen) As Point
        Dim justWorkingArea As Boolean = True

        Dim scrArea_Bounds = screen.Bounds
        Dim scrArea_WorkingArea = screen.WorkingArea

        Trace.WriteLine(String.Format("[Bounds] Left: {0} Right: {1} Top: {2} Bottom: {3} ({4} x {5})", scrArea_Bounds.Left, scrArea_Bounds.Right, scrArea_Bounds.Top, scrArea_Bounds.Bottom, scrArea_Bounds.Width, scrArea_Bounds.Height))
        Trace.WriteLine(String.Format("[WorkingArea] Left: {0} Right: {1} Top: {2} Bottom: {3} ({4} x {5})", scrArea_WorkingArea.Left, scrArea_WorkingArea.Right, scrArea_WorkingArea.Top, scrArea_WorkingArea.Bottom, scrArea_WorkingArea.Width, scrArea_WorkingArea.Height))

        If justWorkingArea Then
            Dim scrArea = screen.WorkingArea

            Select Case crn
                Case corner.LeftTop
                    Return New Point(scrArea.Left, scrArea.Top)
                Case corner.RightTop
                    Return New Point(scrArea.Right - Me.ActualWidth, scrArea.Top)
                Case corner.LeftBottom
                    Return New Point(scrArea.Left, scrArea.Bottom - Me.ActualHeight)
                Case corner.RightBottom
                    Return New Point(scrArea.Right - Me.ActualWidth, scrArea.Bottom - Me.ActualHeight)
                Case Else

            End Select
        Else
            Dim scrArea = screen.Bounds

            Select Case crn
                Case corner.LeftTop
                    Return New Point(scrArea.Left, scrArea.Top)
                Case corner.RightTop
                    Return New Point(scrArea.Right - Me.ActualWidth, scrArea.Top)
                Case corner.LeftBottom
                    Return New Point(scrArea.Left, scrArea.Bottom - Me.ActualHeight)
                Case corner.RightBottom
                    Return New Point(scrArea.Right - Me.ActualWidth, scrArea.Bottom - Me.ActualHeight)
                Case Else

            End Select
        End If


    End Function

    'Function getRightPos(screen As Windows.Forms.Screen) As Point
    '    Dim area_work = screen.WorkingArea

    '    Return New Point(area_work.Right - Me.ActualWidth, area_work.Bottom - Me.ActualHeight)
    'End Function

    Private Sub timer_Tick(sender As Object, e As EventArgs) Handles timer.Tick
        Dim actTime As Date = DateAndTime.Now

        'ActualTime.Text = actTime.ToString("H:mm:ss")
        ActualTime_H.Text = actTime.Hour.ToString 'actTime.ToString("H")
        ActualTime_M.Text = actTime.ToString("mm")
        ActualTime_S.Text = actTime.ToString("ss")
        ActualTime_MS.Text = actTime.ToString("fff")
        ActualDate.Text = actTime.ToString("d. M. yyyy")
        ShortDate.Text = actTime.ToString("ddd").ToUpper
    End Sub


    Private Sub Clock_SizeChanged(sender As Object, e As SizeChangedEventArgs) Handles Me.SizeChanged
        'Trace.WriteLine("[SizeChanged]")

        Locate()
    End Sub

    Private Sub crl_ContentRendered(sender As Object, e As EventArgs) Handles crl.ContentRendered
        Me.ClickThru = Settings.StringToBool(Settings.FromSettings(Settings.Names.ClickThru))
        WindowsServices.DisableMaximize(WindowsServices.GetHandleFromWindow(Me))
    End Sub
End Class
