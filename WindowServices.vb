﻿Imports System.Runtime.InteropServices
Imports System.Windows.Interop

Public Class WindowsServices
    Const GWL_STYLE As Integer = -16
    Const GWL_EXSTYLE As Integer = (-20)

    Const WS_EX_TRANSPARENT As Integer = &H20
    Const WS_MAXIMIZEBOX As Integer = &H10000

    <DllImport("user32.dll")> _
    Private Shared Function GetWindowLong(hwnd As IntPtr, index As Integer) As Integer
    End Function

    <DllImport("user32.dll")> _
    Private Shared Function SetWindowLong(hwnd As IntPtr, index As Integer, newStyle As Integer) As Integer
    End Function

    Public Shared Function GetHandleFromWindow(wnd As Window) As IntPtr
        Return New WindowInteropHelper(wnd).Handle
    End Function

    Public Shared Sub ClickThru(hwnd As IntPtr)
        Dim actStyle = GetWindowLong(hwnd, GWL_EXSTYLE)
        SetWindowLong(hwnd, GWL_EXSTYLE, actStyle Or WS_EX_TRANSPARENT)
    End Sub

    Public Shared Sub notclickThru(hwnd As IntPtr)
        Dim actStyle As Integer = GetWindowLong(hwnd, GWL_EXSTYLE)
        SetWindowLong(hwnd, GWL_EXSTYLE, actStyle And Not WS_EX_TRANSPARENT)
    End Sub

    Public Shared Sub DisableMaximize(hwnd As IntPtr)
        Dim actStyle = GetWindowLong(hwnd, GWL_STYLE)
        SetWindowLong(hwnd, GWL_STYLE, actStyle And Not WS_MAXIMIZEBOX)
    End Sub
End Class