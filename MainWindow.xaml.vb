﻿Imports System.Windows.Forms
Imports Microsoft.Win32

Class MainWindow

    Class Flags
        Public Shared ReadOnly Property SilentStart
            Get
                Return "/silent"
            End Get
        End Property
    End Class

    WithEvents Notify As NotifyIcon
    WithEvents Clock As Clock

    ReadOnly Property Screens As Screen()
        Get
            Return Screen.AllScreens
        End Get
    End Property

    ReadOnly Property StartupRegKeyName As String
        Get
            Return "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"
        End Get
    End Property

    ReadOnly Property StartupRightValue As String
        Get
            Return String.Format("{0} {1}", AppLocation, Flags.SilentStart)
        End Get
    End Property

    ReadOnly Property AppName As String
        Get
            Return My.Application.Info.AssemblyName
        End Get
    End Property

    ReadOnly Property AppLocation As String
        Get
            Return Diagnostics.Process.GetCurrentProcess.MainModule.FileName
        End Get
    End Property

    Private Sub ShowClock_Checked(sender As Object, e As RoutedEventArgs)
        Clock.Visibility = If(ShowClock.IsChecked, Visibility.Visible, Visibility.Collapsed)
    End Sub

    Private Sub DoTopMost_Checked(sender As Object, e As RoutedEventArgs)
        Dim val = DoTopMost.IsChecked

        Clock.Topmost = val
        Settings.ToSettings(Settings.Names.TopMost, Settings.BoolToString(val))
    End Sub

    Private Sub DoMove_Checked(sender As Object, e As RoutedEventArgs)
        Clock.CanMove = DoMove.IsChecked
    End Sub

    Private Sub DoResize_Checked(sender As Object, e As RoutedEventArgs)
        'Clock.CanResize = DoResize.IsChecked
    End Sub
    Sub LoadMonitors()
        ShowClock_Monitor.Items.Clear()

        Dim index As Integer = 0
        For Each scr As Screen In Screen.AllScreens
            Dim itm As New ListBoxItem
            With itm
                .Content = String.Format("[{3}] Velikost obrazovky: {0}x{1}, Výchozí: {2}, GDI jméno: {4}", scr.Bounds.Width, scr.Bounds.Height, If(scr.Primary, "Ano", "Ne"), index, scr.DeviceName)
                .Tag = scr
            End With

            ShowClock_Monitor.Items.Add(itm)

            index += 1
        Next
    End Sub

    Function GetIcon() As System.Drawing.Icon
        Dim iconStream As IO.Stream = Application.GetResourceStream(New Uri("pack://application:,,,/TehDesk;component/icon.ico")).Stream
        Return New System.Drawing.Icon(iconStream)
    End Function

    Private Sub MainWindow_Closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Me.Closing
        Settings.Save()
        'System.Threading.Thread.Sleep(100)
    End Sub

    Private Sub MainWindow_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Notify = New NotifyIcon With {.Icon = GetIcon(), .Text = "TehDesk CP" + vbNewLine + "Pro obnovení, klikněte na tuto ikonu", .Visible = False}

        LoadMonitors()
        Settings.Load()
        ProcessStartupArgs()

        Clock = New Clock
        NiceChrome(Clock, Clock.HolderBorder.BorderThickness, 0)

        ShowClock.IsChecked = True
        DoMilisec.IsChecked = Settings.StringToBool(Settings.FromSettings(Settings.Names.ShowMiliseconds))
        DoTopMost.IsChecked = Settings.StringToBool(Settings.FromSettings(Settings.Names.TopMost)) 'If(Settings.FromSettings(Settings.Names.TopMost) = "True", True, False) 'Clock.Topmost
        DoClickThru.IsChecked = Settings.StringToBool(Settings.FromSettings(Settings.Names.ClickThru))
        DoMove.IsChecked = Clock.CanMove
        Clock_transparency.Value = Val(Settings.FromSettings(Settings.Names.Transparency)) 'Clock.Transparency
        'DoResize.IsChecked = Clock.CanResize

        Clock.ScreenCorner = [Enum].Parse(GetType(Clock.corner), Settings.FromSettings(Settings.Names.Position))
        ShowClock_Corner.SelectedItem = (From elem As ComboBoxItem In ShowClock_Corner.Items Where elem.Tag = Clock.ScreenCorner.ToString).FirstOrDefault

        Dim startTime = DateAndTime.Now
        StartupRegCheck()
        Dim endTime = DateAndTime.Now - startTime
        Trace.WriteLine("MS dif: " + endTime.ToString("fff") + " ms")
    End Sub

    Sub ProcessStartupArgs()
        Dim args = Environment.GetCommandLineArgs()

        If args.Contains(Flags.SilentStart) Then
            Me.WindowState = Windows.WindowState.Minimized
        End If
    End Sub

    Private Sub Clock_ScreenChanged(e As Clock.ScreenChangeEventArgs) Handles Clock.ScreenChanged
        Trace.WriteLine("[ScreenChange] Index: " + e.Index.ToString)

        Dim scr As Screen = Screens(e.Index) 'Gets Actual Screen

        If Not e.SuccessFul Then LoadMonitors()
        ShowClock_Monitor.SelectedIndex = e.Index
    End Sub

    Private Sub ShowClock_Monitor_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles ShowClock_Monitor.SelectionChanged
        Dim selItem = CType(ShowClock_Monitor.SelectedItem, ListBoxItem) : Dim selIndex = ShowClock_Monitor.SelectedIndex
        If selIndex > -1 Then
            Clock.ShowOnScreen = selIndex
        End If
    End Sub

    Private Sub ShowClock_Monitor_Reload_Click(sender As Object, e As RoutedEventArgs)
        LoadMonitors()

        Dim index As Integer = Clock.ShowOnScreen
        If Not index < 0 And Not index > Screens.Count - 1 Then
            ShowClock_Monitor.SelectedIndex = index
        End If
    End Sub

    Private Sub Notify_MouseClick(sender As Object, e As MouseEventArgs) Handles Notify.MouseClick
        Me.Show()
        Me.WindowState = Windows.WindowState.Normal
        Me.Activate()
        'Me.Focus()
    End Sub

    Private Sub MainWindow_StateChanged(sender As Object, e As EventArgs) Handles Me.StateChanged
        If Me.WindowState = Windows.WindowState.Minimized Then
            Me.Hide()
            Notify.Visible = True

            Notify.ShowBalloonTip(3000, "TehDesk CP", "Tady jsem: Minimalizován", ToolTipIcon.Info)
        ElseIf Me.WindowState = Windows.WindowState.Normal Then
            Notify.Visible = False
        End If
    End Sub

    Private Sub Clock_transparency_ValueChanged(sender As Object, e As RoutedPropertyChangedEventArgs(Of Double)) Handles Clock_transparency.ValueChanged
        Clock.Transparency = Clock_transparency.Value
    End Sub

    Public Sub NiceChrome(wnd As Window, resBorder As Thickness, topMove As Integer)
        Shell.WindowChrome.SetWindowChrome(wnd, New Shell.WindowChrome With {.ResizeBorderThickness = resBorder, .GlassFrameThickness = New Thickness(0), .UseAeroCaptionButtons = False, .CaptionHeight = topMove, .CornerRadius = New CornerRadius(0), .NonClientFrameEdges = Shell.NonClientFrameEdges.None})
    End Sub

    Private Sub ShowClock_Corner_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles ShowClock_Corner.SelectionChanged
        If Not ShowClock_Corner.SelectedIndex = -1 And Not Clock Is Nothing Then
            Dim tag = CType(ShowClock_Corner.SelectedItem, ComboBoxItem).Tag.ToString

            Dim corner As Clock.corner = [Enum].Parse(GetType(Clock.corner), tag)

            Clock.ScreenCorner = corner

            Settings.ToSettings(Settings.Names.Position, tag)
        End If
    End Sub

    Private Function CheckStartReg(name As String) As Boolean
        Return (GetStartReg(name) IsNot Nothing)
    End Function

    Private Function GetStartReg(name As String) As String
        Dim reg = Registry.CurrentUser.OpenSubKey(StartupRegKeyName, False)
        Return reg.GetValue(AppName, Nothing)
    End Function

    Private Sub StartupRegCheck()
        StartWithWindow_Indicator.IsChecked = False

        Dim reg = Registry.CurrentUser.OpenSubKey(StartupRegKeyName, True)
        Dim actVal As String = reg.GetValue(AppName, Nothing) 'GetStartReg(AppName)

        If actVal IsNot Nothing Then 'Nastaveno spuštění při startu
            If Not actVal = StartupRightValue Then
                Trace.WriteLine(String.Format("Přepis z [{0}] na [{1}]", actVal, StartupRightValue))

                reg.SetValue(AppName, StartupRightValue)
            Else
                Trace.WriteLine(String.Format("Správné: [{0}]", actVal))
            End If

            StartWithWindow_Indicator.IsChecked = True
        End If

        'reg.Close()
    End Sub

    Private Sub StartWithWindows_Click(sender As Object, e As RoutedEventArgs) Handles StartWithWindows.Click
        Dim reg = Registry.CurrentUser.OpenSubKey(StartupRegKeyName, True)
        Dim actVal = GetStartReg(AppName)

        If actVal Is Nothing Then
            reg.SetValue(AppName, StartupRightValue)

            StartWithWindow_Indicator.IsChecked = True
        Else
            reg.DeleteValue(AppName, False)

            StartWithWindow_Indicator.IsChecked = False
        End If

        'reg.Close()
    End Sub

    Private Sub DoMilisec_Checked(sender As Object, e As RoutedEventArgs) Handles DoMilisec.Checked, DoMilisec.Unchecked
        If Not Me.IsLoaded Then Exit Sub

        Dim checked = DoMilisec.IsChecked
        Settings.ToSettings(Settings.Names.ShowMiliseconds, Settings.BoolToString(checked))

        If checked Then
            Clock.ShowMS = Windows.Visibility.Visible
        Else
            Clock.ShowMS = Windows.Visibility.Collapsed
        End If
    End Sub

    Private Sub DoClickThru_CheckedChanged(sender As Object, e As RoutedEventArgs)
        If Clock IsNot Nothing Then
            Dim value = DoClickThru.IsChecked

            Clock.ClickThru = value

            Settings.ToSettings(Settings.Names.ClickThru, Settings.BoolToString(value))
        End If
    End Sub
End Class
